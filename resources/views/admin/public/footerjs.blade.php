
<!-- jQuery 3 -->
<script   src="{{ URL::asset('admin/lib/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script   src="{{ URL::asset('admin/lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('admin/lib/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ URL::asset('admin/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ URL::asset('admin/lib/moment/min/moment.min.js') }}"></script>
<script src="{{ URL::asset('admin/lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ URL::asset('admin/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap color picker -->
<script src="{{ URL::asset('admin/lib/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ URL::asset('admin/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- FastClick -->
<script   src="{{ URL::asset('admin/lib/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script   src="{{ URL::asset('admin/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script   src="{{ URL::asset('admin/lib/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script   src="{{ URL::asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script   src="{{ URL::asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->

<script   src="{{ URL::asset('admin/lib/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS -->
<script   src="{{ URL::asset('admin/lib/chart.js/Chart.js') }}"></script>
<!-- SlimScroll -->

<!-- CK Editor -->
<script src="{{ URL::asset('admin/lib/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('common/js/layer/layer.js') }}"></script>
<script src="{{ URL::asset('common/js/ajax-form.js') }}"></script>
