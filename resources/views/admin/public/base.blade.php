<!DOCTYPE html>
<html>
  <head>
       @include('admin.public.head')
  </head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      @include('admin.public.header')

        @include('admin.public.left')
     <!-- Left side column. contains the logo and sidebar -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Version 2.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->


        @include('admin.public.footer')


    </div><!-- ./wrapper -->
        @include('admin.public.footerjs')
    <section class="script">
        @yield('script')
   </section>
</body>
</html>
