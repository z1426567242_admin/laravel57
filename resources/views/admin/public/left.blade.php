 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ URL::asset('admin/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">

        <li class="header">MAIN NAVIGATION</li>


        @foreach($menuList as $menu)

          @if($menu->uri == '' && count($menu->second) == 0)
            <php>continue;</php>
          @endif

        <li class=" treeview  ">

          <a href="#">
            <i class="fa {{ $menu->icon }}"></i> <span>{{ $menu->title }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

           @if(count($menu->second) > 0)
          <ul class="treeview-menu">
                 @foreach($menu->second as $sencond_menu)

                    <li><a href="{{ URL::to('admin/'.$sencond_menu->uri) }}"><i class="fa fa-circle-o"></i>{{ $sencond_menu->title }}</a></li>
                  @endforeach

          </ul>
          @endif
        </li>
        @endforeach


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
