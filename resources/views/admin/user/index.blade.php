@extends('admin.public.base')

@section('content')

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
              <h3 class="box-title"><a href="{{ URL::to('admin/user/create') }}">添加数据</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                </tr>

                </thead>
                <tbody>
                @foreach($users as $val)
                    <tr>
                      <td>{{ $val->username }}</td>
                      <td>{{ $val->name }}</td>
                      <td>{{ $val->created_at }}</td>
                      <td><a href="{{ Route('user.edit',$val->id)}}">编辑</a></td>

                    </tr>
                @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

@endsection


@section('script')

<script>


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    CKEDITOR.replace('editor1')

  })
</script>
@endsection