@extends('admin.public.base')

@section('content')


       <div class="row">
              <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                    @if(isset($user) && $user->id)
                     <form method="post" action="{{ route('user.update',$user->id) }}" class="horizontal">
                      @method('PATCH')
                      @csrf
                    @else
                    <form method="post" action="{{ route('user.store') }}" class="horizontal">
                    @csrf
                    @endif


                    <div class="box-body">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" class="form-control" name="username" value="{{ $user->username ?? '' }}" id="exampleInputEmail1" placeholder="Enter email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" class="form-control" name="name" value="{{ $user->name ?? '' }}" id="exampleInputEmail1" placeholder="Enter email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control"  name="password" id="exampleInputPassword1" placeholder="Password">
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                    <input type="button" value="提交" class="ajax-post btn btn-primary">
                    </div>
                  </form>

                </div>

@endsection


@section('script')

<script>


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    CKEDITOR.replace('editor1')

  })
</script>
@endsection