exports.install = function (Vue, options) {
    Vue.prototype.appid = "wx7c2b186ececbe5aa";		//微信appid
    Vue.prototype.openidUrl = ""		//获取微信opendid路径
    Vue.prototype.Url2 = "https://www.easy-mock.com/mock/5a72c20ac76727050336e0c0";
    Vue.prototype.Url = "http://localhost:8000/api";
    Vue.prototype.$home = "/home";   //个人信息
    Vue.prototype.$create = "/post/create";   //个人信息
    Vue.prototype.$posts = "/posts";   //个人信息
    Vue.prototype.$edit = "/post/edit";   //个人信息
    Vue.prototype.$my = "/my";   //个人信息
}