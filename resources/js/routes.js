

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
import Config from "@/js/config/config.js";

Vue.use(Config);
Vue.use(VueAxios, axios);
//Vue.prototype.$http = axios

import HomeComponent from '@/js/components/HomeComponent.vue';
import CreateComponent from '@/js/components/CreateComponent.vue';
import IndexComponent from '@/js/components/IndexComponent.vue';
import EditComponent from '@/js/components/EditComponent.vue';
import Login from '@/js/layouts/login.vue';
import Reg from '@/js/layouts/reg.vue';

import My from '@/js/views/My.vue';


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'home',
            path: '/',
            component: HomeComponent
        },
        {
            name: 'login',
            path: '/login',
            component: Login
        },
        {
            name: 'reg',
            path: '/reg',
            component: Reg
        },
        {
            name: 'create',
            path: '/create',
            // 路由元信息 meta
            meta: {
                requireAuth: true // 添加该字段，表示进入这个路由是需要登录的
            },
            component: CreateComponent
        },
        {
            name: 'posts',
            path: '/posts',
            // 路由元信息 meta
            component: IndexComponent
        },
        {
            name: 'edit',
            path: '/edit/:id',
            // 路由元信息 meta
            meta: {
                requireAuth: true // 添加该字段，表示进入这个路由是需要登录的
            },
            component: EditComponent
        },
        {
            name: 'my',
            path: '/my',
            // 路由元信息 meta
            meta: {
                requireAuth: true // 添加该字段，表示进入这个路由是需要登录的
            },
            component: My
        }
    ]
});


// 设置路由拦截
// 在vue-router的全局钩子中设置拦截
// 每个路由皆会的钩子函数
// to 进入 from 离开 next 传递
router.beforeEach((to, from, next) => {
    let token = localStorage.getItem('token')
    if (to.meta.requireAuth) {
    if (token) {
        next()
    } else {
        next({
            path: '/login',
            query: {
                redirect: to.fullPath
            }
        })
    }
} else {
    next()
}
})

export default router;


