/**
 * from表单提交
 * */

//form表达提交
$(".ajax-post").click(function(){
    var data,ajaxCallUrl,postUrl;

    //解决ckeditor编辑器 ajax上传问他
    if(typeof CKEDITOR=="object"){
        for(instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].updateElement();
        }
    }
    d = $(this).parents('.horizontal');
    postUrl = $(this).attr('post-url');

    //按钮上的url优先
    ajaxCallUrl = postUrl ? postUrl : d.attr('action');

    layer.confirm('是否执行当前操作?', {icon: 3, title:'提示'}, function(index){
        //do something

        $.ajax({
            url : ajaxCallUrl,
            type : 'post',
            dataType : 'json',
            data : d.serialize(),
            success: function(json) {
                if(json.code == 1){

                    layer.msg(json.msg, {icon: 6})

                    if(json.data){

                        // alert(json.data);
                        if('undefined' == typeof(app)){

                        }else{


                            app.phoneLogin(json.data);
                        }
                    }
                    //  $('#alert').html(alertSuccess(json.msg));
                    //if (confirm('是否离开此页')){
                    setTimeout(function() {

                        if(json.url!='') {

                            window.location.href = json.url;
                        }
                    },800);
                    //  }

                }else if(json.code == 0){
                    layer.msg(json.msg,{icon: 6})
                    // $('#alert').html(alertDanger(json.msg));
                    /* if(json.url){
                     window.location.href=json.url;
                     }*/


                }
                setTimeout(function() {
                    $('.close').click();
                },3e3);
            },
            error:function(xhr){          //上传失败
                layer.msg(xhr.responseText)
                // $('#alert').html(alertDanger(xhr.responseText));
            }
        });


        layer.close(index);
    });

});

//a标签post提交
$('.a-post').click(function(){

    var msg =$(this).attr('post-msg');
    var url =$(this).attr('post-url');

    layer.confirm(msg, {icon: 3, title:'提示'}, function(index){

        $.ajax(
            {
                url : url,
                type : 'post',
                dataType : 'json',
                success : function (json)
                {
                    if(json.code == 1){

                        layer.msg(json.msg)
                        setTimeout(function() {
                             if(json.url){

                                 window.location.href=json.url;

                             }
                        },1000);
                    }else if(json.code == 0){
                        layer.msg(json.msg)
                    }
                    setTimeout(function() {
                        $('.close').click();
                    },3e3);
                },
                error:function(xhr){          //上传失败
                    layer.msg(xhr.responseText)
                 //   $('#alert').html(alertDanger(xhr.responseText));

                }
            });

        layer.close(index);
    });


});




function alertSuccess(data){
    return '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+data+'</div>';
}


function alertDanger(data){
    return '<div class="alert alert-danger" role="alert" style="overflow-y: auto;max-height: 600px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+data+'</div>';
}


