<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\AdminUser;


Route::group(['middleware'=>'api','namespace'=>'Api'], function() {
    Route::get('user',function(){
        return AdminUser::all();
    });
    Route::get('/user/{id}',function($id){
       return AdminUser::find($id);
    });

    Route::post('/user/create',function(Request $request){

        return AdminUser::create($request->all());
    });

});


Route::prefix('auth')->group(function($router) {
    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');


});

Route::middleware('refresh.token')->group(function($router) {
    $router->get('profile','UserController@profile');
});


Route::post('/post/create', 'PostController@store');
Route::get('/post/edit/{id}', 'PostController@edit');
Route::post('/post/update/{id}', 'PostController@update');
Route::delete('/post/delete/{id}', 'PostController@delete');
Route::get('/posts', 'PostController@index');
/*Route::post('/login', 'UsersController@index');
Route::post('/token', 'UsersController@token');
*/

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');



Route::post('/users/create', 'UsersController@store');