<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin','namespace'=>'Admin'], function() {
    Route::get('/',"HomeController@index");
    Route::resource('user', 'UserController');
    //Route::get('user',"UserController@index");
});

/*
Route::group(['middleware'=>'api','prefix' => 'api','namespace'=>'Api'], function() {
    Route::get('user', 'UserController@index');
});*/


Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');

