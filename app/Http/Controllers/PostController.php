<?php

// PostController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PostCollection;
use App\Post;

class PostController extends Controller
{
    public function store(Request $request)
    {
        $post = new Post([
            'title' => $request->get('title'),
            'content' => $request->get('content')
        ]);

        $post->save();

        return $this->vue_msg('操作成功','success');
    }

    public function index()
    {

        $data = [
            [
                'id' => 1,
                'menu' => 'test1',
                'child' => [
                    [
                        'id' => 2,
                        'menu' => 'test2',
                    ],
                    [
                        'id' => 3,
                        'menu' => 'test3',
                    ],

                ]
            ],
            [
                'id' => 4,
                'menu' => 'test4',

            ],
            [
                'id' => 5,
                'menu' => 'test5',
                'child' => [
                    [
                        'id' => 6,
                        'menu' => 'test6',
                    ],
                    [
                        'id' => 7,
                        'menu' => 'test7',
                    ],

                ]
            ]
            ,
            [
                'id' => 8,
                'menu' => 'test8',

            ],
            [
                'id' => 9,
                'menu' => 'test9',

            ],
            [
                'id' => 10,
                'menu' => 'test10',
                'child' => [
                    [
                        'id' => 11,
                        'menu' => 'test11',
                    ],
                    [
                        'id' => 12,
                        'menu' => 'test12',
                    ],

                ]
            ],
        ];

        return $this->vue_msg($data,'success');
        //return new PostCollection(Post::all());
    }

    public function edit($id)
    {
        $post = Post::find($id);


        return $this->vue_msg($post,'success');

       // return response()->json($post);
    }

    public function update($id, Request $request)
    {
        $post = Post::find($id);

        $post->update($request->all());

        return $this->vue_msg('操作成功','success');
    }

    public function delete($id)
    {
        $post = Post::find($id);

        $post->delete();

        return $this->vue_msg('操作成功','success');
    }
}