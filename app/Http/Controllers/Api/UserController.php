<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AdminUser;

class UserController extends Controller
{
    public function index(){

        $users = DB::select('select * from admin_users');

        return response()->json($users);

    }

    public function show($id){

        $user = AdminUser::find($id);
        //$user = DB::select('select * from admin_users where id = ?', [$id]);
        //print_r($user);
        return response()->json($user);

    }
}