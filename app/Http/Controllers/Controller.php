<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ajax_msg($msg,$url='',$code=1,$data=[],$wait=3){

        $ajax_msg = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];

        return response()->json($ajax_msg);
    }


    public function vue_msg($msg,$flag='success'){

        $vue_msg = [
            'flag' => $flag,
            'msg'  => $msg
        ];

        return response()->json($vue_msg);
    }
}
