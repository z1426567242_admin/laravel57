<?php

namespace App\Http\Controllers\Admin;


use App\User;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{
    public function index()
    {
        return view('admin.home.index', ['name' => 'James']);
        //return view('admin.home.index');
    }
}