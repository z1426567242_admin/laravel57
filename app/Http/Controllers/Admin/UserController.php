<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AdminUser;

class UserController extends BaseController
{
    public function index()
    {
        $users = DB::select('select * from admin_users');

        return view('admin.user.index', ['users' => $users]);

    }


    public function create()
    {


        return view('admin.user.add');
    }


    public function edit($id)
    {

        $user = DB::table('admin_users')->where('id', $id)->first();
        //$user = DB::select('select * from admin_users where id = ?', [$id]);
        //print_r($user);
        return view('admin.user.add', ['user' => $user]);

    }


    public function store(Request $request)
    {

        $request->validate([
            'username'=>'required',
            'name'=>'required',
            'password'=> 'required|integer',
        ]);
        $share = new AdminUser([
            'username' => $request->get('username'),
            'name' => $request->get('name'),
            'password'=> $request->get('password'),
        ]);
        $result = $share->save();

        if ($result) {

            return $this->ajax_msg('操作成功','/admin/user');
        }

        return $this->ajax_msg('操作失败','/admin/user',0);
    }


    public function update(Request $request,$id)
    {

        $request->validate([
            'username'=>'required',
            'name'=>'required',
            'password'=> 'required|integer',
        ]);

        $share = AdminUser::find($id);
        $share->username = $request->get('username');
        $share->name = $request->get('name');
        $share->password = $request->get('password');
       $result =  $share->save();
        if ($result) {

            return $this->ajax_msg('操作成功','/admin/user');
        }

        return $this->ajax_msg('操作失败','/admin/user',0);
    }
    
}