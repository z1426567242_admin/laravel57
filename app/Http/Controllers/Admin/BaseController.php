<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Base;


class BaseController extends Controller
{

    public function __construct()
    {
        $uid = 1;
        $roleid = 1;
        $baseModel = new Base;
        $menuList = $baseModel->getMenuListByRole($uid,$roleid);
        view()->share('menuList', $menuList);

    }

}