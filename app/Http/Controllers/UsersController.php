<?php

// PostController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $post = new Users([
            'username' => $request->post('username'),
            'password' => encrypt($request->post('password'))
        ]);

        $result = $post->find($post);

        if($result){
            $data = [
                'token' => '123456'
            ];
            return $this->vue_msg($data,'success');

        }else{
            return $this->vue_msg('操作失败','error');

        }

    }

    public function store(Request $request)
    {
        $post = new Users([
            'name' => $request->post('name'),
            'avatarUrl' => $request->post('avatarUrl'),
            'password' => bcrypt('55555'),
            'email' => '12@qq.com'
        ]);

        $post->save();

        return $this->vue_msg('操作成功','success');
    }

    public function token(){

    }

}