<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     * Users::create(['name' => 'Test','email' => 17623239881,'password' => bcrypt('123456')]);
     * @var array
     */
    protected $fillable = [
        'name', 'password','email','avatarUrl'
    ];

}
