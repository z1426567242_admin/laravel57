<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Base extends Model
{
    /**
     * 查询权限可以看到的功能列表
     * @param uid int 用户的id
     * @param return array 数组
    */
    public function getMenuListByRole($uid,$roleid){
        if($uid == 1){

            $rolemenulist = Db::select("select * from   admin_menu   WHERE admin_menu.`state` = '1' and admin_menu.`parent_id` = '0'  ");

            foreach($rolemenulist as $key=>$val){

                $result_second = Db::select("select * from   admin_menu  WHERE admin_menu.`state` = '1' AND admin_menu.`parent_id` = ?  ",[$val->id]);
                $rolemenulist[$key]->second = $result_second;
            }
        }else{
            $sql = "select * from admin_role_menu INNER JOIN admin_menu ON admin_menu.id = admin_role_menu.menuid WHERE admin_menu.`state` = '1'  AND admin_role_menu.roleid = ? and admin_menu.parent_id=0  order by sortlevel,admin_menu.parent_id asc ";
            $rolemenulist = Db::select($sql,[$roleid]);
            foreach($rolemenulist as $key=>$val){

                $sql = "select * from admin_role_menu INNER JOIN admin_menu ON admin_menu.id = admin_role_menu.menuid WHERE admin_menu.`state` = '1'  AND admin_menu.`parent_id` = ? and  admin_role_menu.role_id = ?   order by sortlevel,admin_menu.level asc ";

                $result_second = Db::select($sql,[$val->id,$roleid]);
                $rolemenulist[$key]->second = $result_second;
            }
        }


        return $rolemenulist;
    }

}
